_Note: I'm working on the master branch - refactoring a large part of libalt,
so things will be broken for a while_

Welcome to the repository for altserv, an attempt at creating custom server
software for [Altitude][].

The project has two main components: libalt, an implementation of altitude's
networking protocol, and altserv, the server software itself.

The goal is to create an extensible server, staying close to that of the
original, but with a few main improvements:

 - More easily modable.
 - Recording and replaying games. Messages and game events are structured
   in a way which should make it trivial to implement server side game recording.
 - One server shouldn't necessarily be a single game.

[altitude]: http://altitudegame.com "Altitude's Website"

## Building altserv

Altserv is written in Rust, and currently depends on rust beta.

```
git clone --recursive git@gitlab.com:xalri/altserv.git
cd altserv/altserv
cargo build
```

This will download and build all the dependencies, then altserv itself.
Linux and windows builds will be available when it's useful enough to release.
