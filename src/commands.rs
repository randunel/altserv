use client::*;
use libalt::event::*;

pub fn run(client: &mut Client, command: String){
    let reply = CmdRespEv{
        command: command,
        result: String::from("Commands are not implemented Q.Q"),
    };
    client.send_ev(Event::CmdResp(reply));
}
