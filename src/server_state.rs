use libalt::server::*;

pub struct ServerState{
    info: ServerInfo,
    conf: ServerConf,
}

impl ServerState{
    pub fn new(c: &Config) -> ServerState{
        let c = c.read();
        let mut info = ServerInfo::default();
        let mut conf = ServerConf::new();

        info.max_players = c.max_players;
        conf.max_players = c.max_players;

        ServerState{
            info: info,
            conf: conf,
        }

    }
}
