use client::*;
use libalt::event::*;
use libalt::client::*;

pub fn say(clients: &ClientMap, player: i32, msg: &str){
    let ev = ChatEv{
        player: PlayerNo{ value: player },
        is_team: false,
        msg: String::from(msg),
    };
    clients.send_ev(Event::Chat(ev));
}

pub fn server_whisp(client: &mut Client, msg: &str){
    let ev = ChatEv{
        player: PlayerNo{ value: -1 },
        is_team: false,
        msg: String::from(msg),
    };
    client.send_ev(Event::Chat(ev));
}
